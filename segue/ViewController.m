//
//  ViewController.m
//  segue
//
//  Created by Click Labs130 on 11/6/15.
//  Copyright (c) 2015 Niteesh Sharma. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@property (strong, nonatomic) IBOutlet UITextField *textField;

@end

@implementation ViewController

@synthesize textField;

- (void)viewDidLoad {
    [super viewDidLoad];
    textField.delegate = self;
    
        // Do any additional setup after loading the view, typically from a nib.
}

-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string{
    
    if ([textField.text isEqualToString:@"1"])
    {
        [self performSegueWithIdentifier:@"controller1" sender:nil];
    }
    else if ([textField.text isEqualToString:@"2"])
    {
        [self performSegueWithIdentifier:@"controller2" sender:nil];
    }
    return YES;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
