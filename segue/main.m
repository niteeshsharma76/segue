//
//  main.m
//  segue
//
//  Created by Click Labs130 on 11/6/15.
//  Copyright (c) 2015 Niteesh Sharma. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
